###############################################################################
########    this snippet needs to be included in top level main.cmd   #########
###############################################################################
# START of mrfioc2.cmd
#
# Timing PCIe EVR 300dc
# As per EVR MTCAPCIe 300 engineering manual ch 5.3.5
# Example found in https://github.com/icshwi/e3-mrfioc2/blob/master/cmds/MTCA-EVR-300_standalone_template.cmd

epicsEnvSet("PREFIX",        "$(LOCATION):")
epicsEnvSet("DEVICE",        "$(EVR_FRU):")
# machine event clock, EVR expects MHz
epicsEnvSet("EVENT_CLOCK"    "88.0525")
# event at 14 Hz (machine rate), EVR expects integer
epicsEnvSet("TRIG_EVT_CODE"  "14")
# pulse in LEBT is ~6 ms, EVR expects us
epicsEnvSet("PULSE_WIDTH"    "1000")
# event to trigger delay, EVR expects us
epicsEnvSet("PULSE_DELAY"    "0")

# var(devPCIDebug, 9)

# see devlib2/pciApp/devLibPCI.c, devPCIFindSpec(): D:B.F, with digits in HEX!
# NOTE: we can use 1st column of lspci output directly
# lspci output for PCIe 300DC EVR:
# 01:00.0 Signal processing controller: Xilinx Corporation XILINX PCI DEVICE
mrmEvrSetupPCI("$(EVR_FRU)", "$(EVR_DEVID)")

# Needed with software timestamp source w/o RT thread scheduling ###
var evrMrmTimeNSOverflowThreshold 100000

# load slighhtly modified DB file that treats SYS and D
# more like areaDetector, with ':' in between, OPI have been adjusted, too
dbLoadRecords("evr-pcie-300dc-ess-bde.db",  "SYS=$(PREFIX), D=$(DEVICE), EVR=$(EVR_FRU), FEVT=$(EVENT_CLOCK), EVNT1HZ=$(TRIG_EVT_CODE)")

# TODO: it would be nice to have this
# set_requestfile_path("$(MRFIOC2)/req")

# END of mrfioc2.cmd
###############################################################################
