###############################################################################
# START of post_init.cmd

# acquisition and data processing configuration
dbpf $(SYSTEM):CAM1-ArrayCallbacks 1
dbpf $(SYSTEM):CAM1-ImageMode 2
dbpf $(SYSTEM):CAM1-DataType 1
dbpf $(SYSTEM):CAM1-BinY $(YSIZE)
dbpf $(SYSTEM):CAM1-TriggerMode 1
dbpf $(SYSTEM):CAM1-ImageMode 2
dbpf $(SYSTEM):CAM1-AndorADCSpeed 0
dbpf $(SYSTEM):CAM1-AndorCooler 1
dbpf $(SYSTEM):CAM1-Temperature -70
dbpf $(SYSTEM):IMG1-EnableCallbacks 1
dbpf $(SYSTEM):TRC1-EnableCallbacks 1
dbpf $(SYSTEM):ROI1-EnableCallbacks 1
dbpf $(SYSTEM):ROI1-EnableScale 1
dbpf $(SYSTEM):PROC1-EnableCallbacks 1
dbpf $(SYSTEM):PROC1-ArrayCallbacks 1
dbpf $(SYSTEM):PROC1-EnableFilter 1
dbpf $(SYSTEM):PROC1-NumFilter 14
dbpf $(SYSTEM):PROC1-FilterType 0
dbpf $(SYSTEM):PROC1-AutoResetFilter 1
dbpf $(SYSTEM):PROC1-FilterCallbacks 1
dbpf $(SYSTEM):STAT1-EnableCallbacks 1
dbpf $(SYSTEM):TIFF1-EnableCallbacks 1
dbpf $(SYSTEM):TIFF1-FilePath "$(HOME)"
dbpf $(SYSTEM):TIFF1-FileName "$(SYSTEM)_Image"
dbpf $(SYSTEM):TIFF1-AutoIncrement 1
dbpf $(SYSTEM):TIFF1-FileTemplate "%s%s-%4.4d.tif"
dbpf $(SYSTEM):FIT1-EnableCallbacks 1
dbpf $(SYSTEM):FIT1-Energy 75
dbpf $(SYSTEM):FIT1-Patience 500

# some defaults; last values from autosave shall be
# overwritten if these are uncommented hence these ignored
# dbpf $(SYSTEM):SPECT-Wavelength 656
# dbpf $(SYSTEM):FIT1-Background 300
# dbpf $(SYSTEM):FIT1-1-PeakAmplitude 10
# dbpf $(SYSTEM):FIT1-1-PeakMu 50
# dbpf $(SYSTEM):FIT1-1-PeakSigma 8
# dbpf $(SYSTEM):FIT1-2-PeakAmplitude 10
# dbpf $(SYSTEM):FIT1-2-PeakMu 110
# dbpf $(SYSTEM):FIT1-2-PeakSigma 9
# dbpf $(SYSTEM):FIT1-3-PeakAmplitude 10
# dbpf $(SYSTEM):FIT1-3-PeakMu 140
# dbpf $(SYSTEM):FIT1-3-PeakSigma 9

### timing configuration

## standalone mode only

# example for standalone mode found at
# https://github.com/icshwi/e3-mrfioc2/blob/master/cmds/MTCA-EVR-300_standalone_template.cmd
# uncomment the lines below to enable EVR built-in sequencer acting as EVG
# comment out when receiving events from EVG

### Get current time from system clock ###
# dbpf $(LOCATION):$(EVR_FRU):TimeSrc-Sel 2

### Set up the prescaler that will trigger the sequencer at 14 Hz ###
# in standalone mode because real freq from synthsiezer is 88.05194802 MHz, otherwise 6289464
# Event rate = clock rate / prescaler
# 14 Hz ~= 88052500 / 6289464
# 1 Hz  ~= 88052500 / 88052500
# dbpf $(LOCATION):$(EVR_FRU):PS0-Div-SP 6289424

### Set up the sequencer ###
# normal mode, rearm after finish
# dbpf $(LOCATION):$(EVR_FRU):SoftSeq0-RunMode-Sel 0
# trigger the sequence from prescaler 0
# dbpf $(LOCATION):$(EVR_FRU):SoftSeq0-TrigSrc:2-Sel 2
# select us as the units of the timestamp array of the seuqncer
# dbpf $(LOCATION):$(EVR_FRU):SoftSeq0-TsResolution-Sel 2
# dbpf $(LOCATION):$(EVR_FRU):SoftSeq0-Load-Cmd 1
# dbpf $(LOCATION):$(EVR_FRU):SoftSeq0-Enable-Cmd 1

### Run the script that configures the events and timestamp of the sequence ###
#system("/bin/sh ./configure_sequencer_14Hz.sh $(LOC):PBI $(EVR_UNIT)")
# XXX: can not use dbpf since it can not handle arrays of numbers..
# system "caput -a $(LOCATION):$(EVR_FRU):SoftSeq0-EvtCode-SP 2 14 127 >/dev/null"
# system "caput -a $(LOCATION):$(EVR_FRU):SoftSeq0-Timestamp-SP 2 0 1 >/dev/null"
# system "caput    $(LOCATION):$(EVR_FRU):SoftSeq0-Commit-Cmd 1 >/dev/null"

## end of standalone mode only

## normal and standalone configuration

### bugfix for EVR loosing the timestamp for 5 seconds every 7-8 hours
dbpf $(LOCATION):$(EVR_FRU):DC-Tgt-SP 70

### trigger setup
dbpf $(LOCATION):$(EVR_FRU):DlyGen0-Evt-Trig0-SP $(TRIG_EVT_CODE)
# trigger pulse width in us resolution (see SoftSeq0-TsResolution-Sel)
dbpf $(LOCATION):$(EVR_FRU):DlyGen0-Width-SP $(PULSE_WIDTH)
# trigger from delay generator 0
dbpf $(LOCATION):$(EVR_FRU):DlyGen0-Delay-SP $(PULSE_DELAY)
# trigger front panel UNIV trigger line XX at 14 Hz
#  FP UNIV line XX; X=00..15
#  dbpf $(LOCATION):$(EVR_FRU):OutFPUVXX-Src-SP 0
# first 4 front panel outputs get trigger source set to pulser 0
# FP UNIV 00
dbpf $(LOCATION):$(EVR_FRU):OutFPUV00-Src-SP 0
# FP UNIV 01
dbpf $(LOCATION):$(EVR_FRU):OutFPUV01-Src-SP 0
# FP UNIV 02
dbpf $(LOCATION):$(EVR_FRU):OutFPUV02-Src-SP 0
# FP UNIV 03
dbpf $(LOCATION):$(EVR_FRU):OutFPUV03-Src-SP 0

## normal and standalone configuration

## setup timestamping

# raw data and counter of acquired images
dbpf $(SYSTEM):IMG1-ArrayData.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):CAM1-ArrayCounter_RBV.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
# fitting results
dbpf $(SYSTEM):FIT1-1-PeakAmplitudeActual.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-1-PeakSigmaActual.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-1-PeakMuActual.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-2-PeakAmplitudeActual.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-2-PeakSigmaActual.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-2-PeakMuActual.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-3-PeakAmplitudeActual.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-3-PeakSigmaActual.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-3-PeakMuActual.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-BackgroundActual.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
# raw and fitted trace
dbpf $(SYSTEM):FIT1-Fit_RBV.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-Y_RBV.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
# fractions
dbpf $(SYSTEM):FIT1-FracH2.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-FracH3.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-FracProton.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
# fitting counters
dbpf $(SYSTEM):FIT1-GoodCounter_RBV.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-NrIterations_RBV.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME
dbpf $(SYSTEM):FIT1-BadCounter_RBV.TSEL $(LOCATION):$(EVR_FRU):Time-I.TIME

# END of post_init.cmd
###############################################################################
