###############################################################################
########    this snippet needs to be included in top level main.cmd   #########
###############################################################################
# START of adandor.cmd
#
# Andor camera & spectrograph for Doppler Shift Monitor (LEBT)
#

epicsEnvSet("PREFIX",        "$(SYSTEM):")
epicsEnvSet("PORT",          "CAM")
epicsEnvSet("XSIZE",         "1024")
epicsEnvSet("YSIZE",         "255")
# NELEM: maximum number of points to acquire
#        NELEM = XSIZE * YSIZE+1
epicsEnvSet("NELEM"     ,    "262144")
epicsEnvSet("NCHANS",        "2048")
epicsEnvSet("QSIZE",         "20")

#
# Image processing pipeline (asyn port names):
#
#                        CAM
#                         |
#    -------------------------------------------
#    |         |          |          |         |
#   IMG1      TRC1      TIFF1       ROI1     STAT1
#    |         |          |          |
#   2-D       1-D         |          |
# waveform  waveform   .tiff       PROC1
#                                    |
#                                   FIT1
#

############### Andor SDK kludges
# clearing the SHM entries that Andor SDK created to avoid getting SDK 20992 error (see README.md)
system("ipcrm --shmem-key 0xffffffff")
# sleeping 5 seconds in order for camera to recover if IOC was restarted
system("sleep 5")
############### continue normal startup

# Create an andorCCD driver
# andorCCDConfig(const char *portName, const char *installPath, int shamrockID,
#                int maxBuffers, size_t maxMemory, int priority, int stackSize)
andorCCDConfig("$(PORT)", "$(ADANDOR)/etc/andor/", 0, 0, 0, 0, 0)
dbLoadRecords("andorCCD.template",      "P=$(PREFIX), R=CAM1-,  PORT=$(PORT),ADDR=0,TIMEOUT=1")
# SHAMROCK spectrometer
# shamrockConfig(const char *portName, int shamrockId, const char *iniPath, int priority, int stackSize)
shamrockConfig("SPECT", 0, "", 0, 0)
dbLoadRecords("shamrock.template",      "P=$(PREFIX), R=SPECT-, PORT=SPECT,TIMEOUT=1,PIXELS=1024")
# Image waveform plugin
NDStdArraysConfigure("IMG1", $(QSIZE), 0, "$(PORT)", 0, 0)
dbLoadRecords("NDStdArrays.template",   "P=$(PREFIX), R=IMG1-,  PORT=IMG1,ADDR=0,TIMEOUT=1,TYPE=Int32,FTVL=ULONG, NELEMENTS=$(NELEM),NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
# Trace waveform plugin
NDStdArraysConfigure("TRC1", $(QSIZE), 0, "$(PORT)", 0, 0)
dbLoadRecords("NDStdArrays.template",   "P=$(PREFIX), R=TRC1-,  PORT=IMG1,ADDR=0,TIMEOUT=1,TYPE=Int32,FTVL=ULONG,NELEMENTS=$(XSIZE),NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
# TIFF file saving plugin
NDFileTIFFConfigure("TIFF1", $(QSIZE), 0, "$(PORT)", 0)
dbLoadRecords("NDFileTIFF.template",    "P=$(PREFIX), R=TIFF1-, PORT=TIFF1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")
# ROI plugin
NDROIConfigure("ROI1", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("NDROI.template",         "P=$(PREFIX), R=ROI1-,  PORT=ROI1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")
# PROC plugin
NDProcessConfigure("PROC1", $(QSIZE), 0, "ROI1", 0, 0, 0)
dbLoadRecords("NDProcess.template",     "P=$(PREFIX), R=PROC1-, PORT=PROC1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=ROI1")
# NDFits plugin
NDFitsConfigure("FITS1", $(QSIZE), 0, "PROC1", 0, 3, 0, 0, 0)
dbLoadRecords("dopplerFits.template",   "P=$(PREFIX), R=FIT1-,  PORT=FITS1,ADDR=0,TIMEOUT=1,XSIZE=$(XSIZE),YSIZE=$(YSIZE), NCHANS=$(NCHANS),NDARRAY_PORT=PROC1")
dbLoadRecords("NDFitsN.template",       "P=$(PREFIX), R=FIT1-1-,PORT=FITS1,ADDR=0,TIMEOUT=1,NCHANS=$(NCHANS)")
dbLoadRecords("NDFitsN.template",       "P=$(PREFIX), R=FIT1-2-,PORT=FITS1,ADDR=1,TIMEOUT=1,NCHANS=$(NCHANS)")
dbLoadRecords("NDFitsN.template",       "P=$(PREFIX), R=FIT1-3-,PORT=FITS1,ADDR=2,TIMEOUT=1,NCHANS=$(NCHANS)")
# Statistics plugin
NDStatsConfigure("STAT1", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("NDStats.template",       "P=$(PREFIX), R=STAT1-, PORT=STAT1,ADDR=0,TIMEOUT=1,HIST_SIZE=256, XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(XSIZE),NDARRAY_PORT=$(PORT)")

set_requestfile_path("./")
set_requestfile_path("$(ADANDOR)/req")
set_requestfile_path("$(ADMISC)/req")
set_requestfile_path("$(ADCORE)/req")
set_requestfile_path("$(BUSY)/req")
set_requestfile_path("$(SSCAN)/req")
set_requestfile_path("$(CALC)/req")
set_savefile_path("./autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")
save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("$(AUTOSAVE)/db/save_restoreStatus.db", "P=$(PREFIX)")

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)

# silence FITS plugin
asynSetTraceIOMask("FITS1",0,0)
asynSetTraceMask("FITS1",0,0)

# END of adandor.cmd
###############################################################################
