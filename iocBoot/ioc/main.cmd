###############################################################################
########    this snippet needs to be included in top level st.cmd     #########
###############################################################################
# START of main.cmd

errlogInit(20000)

dbLoadDatabase("$(TOP)/dbd/dsmApp.dbd")
dsmApp_registerRecordDeviceDriver(pdbbase)

# we need caput and caRepeater
epicsEnvSet(PATH, $(PATH):$(EPICS_BASE)/bin/linux-x86_64)

< adandor.cmd

< mrfioc2.cmd

###############################################################################
iocInit()
###############################################################################

# save things every thirty seconds
create_monitor_set("auto_settings.req", 30,"P=$(SYSTEM):")

< post_init.cmd

# END of main.cmd
###############################################################################
