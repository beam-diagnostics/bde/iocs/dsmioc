###############################################################################
#
#  Development Doppler Shift Monitor (LEBT)
#
#  Support: https://jira.esss.lu.se/projects/WP7DSM
#
###############################################################################


###############################################################################
# START of st.cmd

< envPaths

# LOCATION: location of the system (section-subsection)
epicsEnvSet("LOCATION",                     "LAB-010")
# INSTANCE: PBI device name (discipline-device-index)
epicsEnvSet("INSTANCE",                     "PBI-Dpl-001")
# SYSTEM: system name (LOCATION:SYSTEM)
epicsEnvSet("SYSTEM",                       "$(LOCATION):$(INSTANCE)")
# EVR_FRU: EVR device name for this system/IOC (discipline-device-index)
epicsEnvSet("EVR_FRU",                      "PBI-EVR-Dpl01")
# EVR_DEVID: EVR device PCI ID (from lspci)
epicsEnvSet("EVR_DEVID",                    "01:00.0")
# Note: above values should come from ESS Naming service

# EPICS_CA_MAX_ARRAY_BYTES: 10 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
# EPICS_DB_INCLUDE_PATH: list all module db folders
epicsEnvSet("EPICS_DB_INCLUDE_PATH",        "$(TOP)/db:$(ADCORE)/db:$(ADANDOR)/db:$(ADMISC)/db:$(MRFIOC2)/db")

< main.cmd

# END of st.cmd
###############################################################################
