# DSM IOC

Doppler Shift Monitor IOC

## List of PVs

List of PVs (with all macros expanded) is available in files like `pvs_Rxxx`.

## Camera & spectrograph issues

### Shared memory segment

It may happen that AndorSDK created /dev/shm entry gets corrupt after unclean IOC shutdown.
In such case any subsequent IOC attempts to talk to the camera fail with AndorSDK error as follows:

	andorCCDConfig("CAM", "/opt/bde/R3.15.5/adandor-R2-7/etc/andor/", 0, 0, 0, 0, 0)
	andorCCD:AndorCCD: initializing camera
	2018/08/27 12:01:20.153 andorCCD:AndorCCD: ERROR: Unknown error code=20992 returned from Andor SDK.

This error state persists until IPC is restarted.

After some googling it turns out we can use ipcXXX utilities to inspect shared memory status.


Solution is to remove the shared memory entry before IOC starts!

Use this command in iocsh() via 'system' command:

	ipcrm --shmem-key 0xffffffff

#### Details

If IOC would be normally stopped via 'exit' then the SHM entry is remove from the system (ipcs shows nothing).
If IOC is stopped with a signal CTRL + C / D then the SHM entry remains, but IOC (always?) able to recover communication with the camera.
If 'kill -9 <PID>' is used, then we start getting the Andor SDK error(s); mostly 20992 - meaning DRV_NOT_AVAILABLE.

The error response comes quickly, without any USB communication involved. This hints of pure software SDK issue; not camera related/specific.

By looking at the libandor.so one can notice the usage of linux SHM concepts. It is assumed due to the fact that many cameras might be attached and each is controlled by the library inside its 'camera context'. 

This SHM info is seen after IOC has started (successfully or not):

	[dev@dsm-ipc ~]$ ipcs 

	------ Message Queues --------
	key        msqid      owner      perms      used-bytes   messages    

	------ Shared Memory Segments --------
	key        shmid      owner      perms      bytes      nattch     status      
	0xffffffff 98304      dev        666        488        0                       

	------ Semaphore Arrays --------
	key        semid      owner      perms      nsems     

We can remove the SHM segment, if IOC keeps getting Andor SDK 20992 error, with following:

	[dev@dsm-ipc ~]$ ipcrm --shmem-key 0xffffffff

And if we list the shared memory segments again, it should be empty:

	[dev@dsm-ipc ~]$ ipcs 
	------ Message Queues --------
	key        msqid      owner      perms      used-bytes   messages    

	------ Shared Memory Segments --------
	key        shmid      owner      perms      bytes      nattch     status      

	------ Semaphore Arrays --------
	key        semid      owner      perms      nsems     


If IOC is now started it should be possible to talk to the camera again without reboot of OS or power cycle of PC and/or camera.

## Stuck grating

During development it was observed many times that the control of the grating in the spectrograph is not possible. The built-in microcontroller that talks over USB is not performing movement of the grating if instructed so via software request.

Solution is to power cycle the shamrock spectrograph unit and then restart the IOC to regain control over the grating movement.
